namespace Data_Layer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sec : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ZaEmpolyees", "Age", c => c.Int());
            AddColumn("dbo.ZaEmpolyees", "Gender", c => c.Boolean());
            AddColumn("dbo.ZaEmpolyees", "Social_status", c => c.Boolean());
            AddColumn("dbo.ZaEmpolyees", "Place_of_birth", c => c.String());
            AddColumn("dbo.ZaEmpolyees", "Residence", c => c.String());
            AddColumn("dbo.ZaEmpolyees", "BirthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ZaEmpolyees", "BirthDate");
            DropColumn("dbo.ZaEmpolyees", "Residence");
            DropColumn("dbo.ZaEmpolyees", "Place_of_birth");
            DropColumn("dbo.ZaEmpolyees", "Social_status");
            DropColumn("dbo.ZaEmpolyees", "Gender");
            DropColumn("dbo.ZaEmpolyees", "Age");
        }
    }
}
