namespace Data_Layer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZaEmpolyees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullNameAR = c.String(),
                        FullNameEN = c.String(),
                        NumOfId = c.String(),
                        Direction = c.String(),
                        Release_Date = c.DateTime(),
                        Expiry_date = c.DateTime(),
                        Number_of_insurance = c.String(),
                        Insurance_Authority = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ZaEmpolyees");
        }
    }
}
