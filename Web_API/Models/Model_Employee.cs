﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API.Models
{
    public class Model_Employee
    {
        public int Id { get; set; }
        public string FullNameAR { get; set; }
        public string FullNameEN { get; set; }
        public int? Age { get; set; }
        public bool? Gender { get; set; }
        public bool? Social_status { get; set; }
        public string Place_of_birth { get; set; }
        public string Residence { get; set; }
        public DateTime? BirthDate { get; set; }
        public string NumOfId { get; set; }
        public string Direction { get; set; }
        public DateTime? Release_Date { get; set; }
        public DateTime? Expiry_date { get; set; }
        public string Number_of_insurance { get; set; }
        public string Insurance_Authority { get; set; }
    }
}