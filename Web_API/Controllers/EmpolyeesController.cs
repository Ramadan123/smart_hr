﻿
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Data_Layer;
using Data_Layer.Model;

namespace Web_API.Controllers
{
    public class EmpolyeesController : ApiController
    {
        private Context db = new Context();

        // GET: api/Empolyees
        public IQueryable<ZaEmpolyee> GetzaEmpolyees()
        {
            return db.zaEmpolyees;
        }

        // GET: api/Empolyees/5
        [ResponseType(typeof(ZaEmpolyee))]
        public IHttpActionResult GetZaEmpolyee(int id)
        {
            ZaEmpolyee zaEmpolyee = db.zaEmpolyees.Find(id);
            if (zaEmpolyee == null)
            {
                return NotFound();
            }

            return Ok(zaEmpolyee);
        }

        // PUT: api/Empolyees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZaEmpolyee(int id, ZaEmpolyee zaEmpolyee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zaEmpolyee.Id)
            {
                return BadRequest();
            }

            db.Entry(zaEmpolyee).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZaEmpolyeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Empolyees
        [ResponseType(typeof(ZaEmpolyee))]
        public IHttpActionResult PostZaEmpolyee(ZaEmpolyee zaEmpolyee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.zaEmpolyees.Add(zaEmpolyee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zaEmpolyee.Id }, zaEmpolyee);
        }

        // DELETE: api/Empolyees/5
        [ResponseType(typeof(ZaEmpolyee))]
        public IHttpActionResult DeleteZaEmpolyee(int id)
        {
            ZaEmpolyee zaEmpolyee = db.zaEmpolyees.Find(id);
            if (zaEmpolyee == null)
            {
                return NotFound();
            }

            db.zaEmpolyees.Remove(zaEmpolyee);
            db.SaveChanges();

            return Ok(zaEmpolyee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ZaEmpolyeeExists(int id)
        {
            return db.zaEmpolyees.Count(e => e.Id == id) > 0;
        }
    }
}